#include <utils/fs.h>
#include <fstream>
#include <iostream>
#include "globals.h"

const size_t utils::fs::MaxFileSizeInBytes = 1024*1024*2;//макимальный размер файла = 2Мб

bool utils::fs::isFileExists(const std::string& filename)
{
    return (std::ifstream(filename.c_str()) != NULL);
}
/*
bool utils::fs::isFileNameCorrect(const std::string& filename)
{
    bool res(false);
    try{
        res = res || isFileExists(filename);
        res = res || std::ofstream::
    }catch(std::exception& e){
        std::cout << FUNC_INFO(e.what());
    }
    return res;
}
*/

size_t utils::fs::fileSizeInBytes(const std::string& filename)
    throw(std::runtime_error)
{
    size_t res(0);
    std::ifstream file(filename.c_str(), std::ios_base::binary);
    if(file){
        try{
            file.seekg(0, std::ios_base::end);
            res = file.tellg();
            file.close();
        }catch(std::exception &e){
            throw std::runtime_error(FUNC_INFO("Can't get size of file "+filename+" ("+e.what()+")"));
        }
    }else{
        throw std::runtime_error(FUNC_INFO("Can't open file "+filename));
    }
    return res;
}

//TODO!!! описание
std::string utils::fs::generateNumberedFileName(const std::string& prefix, size_t& currn, size_t stopn, size_t nstrlen, const std::string& suffix)
    throw(std::runtime_error)
{
    std::string res;
    if(currn <= stopn){
	std::string currn_str;
	do{
	    if(currn > stopn){
		throw std::runtime_error(FUNC_INFO("Reached upper bound!"));
	    }
	    currn_str = utils::string::toStdString(currn++);
	    if(currn_str.length()<nstrlen){
		currn_str.insert(currn_str.begin(), nstrlen - currn_str.length(), '0');//приводим строку, содержащую номер файла к указанной длине
	    }
	    res = prefix + currn_str + suffix;
	}while(utils::fs::isFileExists(res));
    }else{
	throw std::runtime_error(FUNC_INFO("Parameter 'currn' must be less than or equal to parameter 'stopn'"));
    }
    return res;
}

