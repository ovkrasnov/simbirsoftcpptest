#include <utils/wstring.h>
#include <iostream>
#include <locale>
#include "globals.h"
#include <stdio.h>
#include <wctype.h>
#include <utils/misc.h>

bool utils::wstring::isNotWordDelimiter(wchar_t c){
    bool res(false);
    res = res || iswalpha(c);
    res = res || iswdigit(c);
    res = res || (wchar_t('_') == c);//считаем одним словом строки вида word1_word2 //TODO!!! протестить, правильно ли работает
    return res;
}

bool utils::wstring::isWordDelimiter(wchar_t c)
{
    return !isNotWordDelimiter(c);
}

bool utils::wstring::isNotSentenceDelimiter(wchar_t c)
{
    return !isSentenceDelimiter(c);
}

bool utils::wstring::isSentenceDelimiter(wchar_t c)
{
    bool res(false);
    res = res || (wchar_t('.')==c);//TODO!!! протестить, правильно ли работает
    res = res || (wchar_t('?')==c);//TODO!!! протестить, правильно ли работает
    res = res || (wchar_t('!')==c);//TODO!!! протестить, правильно ли работает
    return res;
}

typedef std::codecvt<wchar_t, char, mbstate_t> codec_vt_t;

bool utils::wstring::fillWString(std::wstring* dst, const std::string& src, const std::locale& src_locale)
{
    bool res(true);
    res = res && dst;
    if(res){
	std::mbstate_t state;
	const  codec_vt_t& codec_vt = std::use_facet<codec_vt_t>(src_locale);
	wchar_t* wchar_array = new wchar_t[src.size()+1];
	const char* src_next = 0;
	wchar_t* dst_next = 0;
	std::codecvt_base::result r = codec_vt.in(state, src.c_str(), src.c_str()+src.size(), src_next, wchar_array, wchar_array+src.size()+1, dst_next);
	switch(r){
	    case std::codecvt_base::ok:
	    case std::codecvt_base::noconv:
		*dst_next = '\0';
		*dst = wchar_array;
		break;
	    default:
		std::cerr << FUNC_INFO(std::string() + "Error! An error occured while converting from encoding '" + src_locale.name() + "' to std::wstring!");
		res = false;
		break;
	}
	delete[] wchar_array;
    }else{
	std::cerr << FUNC_INFO("Error! Parameter 'dst' is null!");
    }
    return res;
}

bool utils::wstring::fillString(std::string* dst, const std::wstring& src, const std::locale& dst_locale)
{
    bool res(true);
    res = res && dst;
    if(res){
	std::mbstate_t state;
	const codec_vt_t& codec_vt = std::use_facet<codec_vt_t>(dst_locale);
	std::wstring::size_type length = src.size() << 8;//на всякий случай с большим запасом
	char* char_array = new char[length+1];
	const wchar_t* src_next = 0;
	char* dst_next = 0;
	std::codecvt_base::result r = codec_vt.out(state, src.c_str(), src.c_str()+src.size(), src_next, char_array, char_array + length, dst_next);
	switch(r){
	    case std::codecvt_base::ok:
	    case std::codecvt_base::noconv:
		*dst_next = '\0';
		*dst = char_array;
		break;
	    default:
		std::cerr << FUNC_INFO(std::string() + "Error! An error occured while converting to encoding '" + dst_locale.name() += "' to std::string!");
		res = false;
		break;
	}
	
	delete[] char_array;
    }
    return res;
}

bool utils::wstring::replace(std::wstring* str, const std::wstring& what, const std::wstring& with)
{
    bool res(true);
    res = res && str;
    if(res){
	for(
	    std::wstring::size_type i = str->find(what, 0);
	    i != std::wstring::npos;
	    i = str->find(what, i + with.size())
	){
	    str->replace(i, 1, with);
	}
    }else{
	std::cerr << FUNC_INFO("Warning! Pointer to source is null!");
    }
    return res;
}
