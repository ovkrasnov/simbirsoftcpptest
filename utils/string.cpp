#include <utils/string.h>
#include <iostream>
#include "globals.h"
#include <stdio.h>
#include <wctype.h>
#include <utils/misc.h>

//const std::wstring utils::wstring::Delimiters = "\t ,;:().";

int utils::string::toInt(const std::string& value, bool* ok)
{
    //std::cout << "converting to int value '" << value <<"'\n";
    int res(0);
    try{
	std::istringstream ist(value);
	ist >> res;
	if(ok){
	    *ok = true;
	}
    }catch(std::exception& e){
	if(ok){
	    *ok= false;
	}
	//std::wcerr << FUNC_INFO(
	std::cerr << FUNC_INFO(std::string()
	    + "An error occured while converting string '"
	    + value
	    + "' to int:'"
	    + e.what()
	    + "'!\n"
	);
    }
    //std::cout << "converting to int value '" << value <<"' res=[" <<res << "]\n";
    return res;
}


