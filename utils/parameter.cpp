#include <utils/parameter.h>
#include <utils/misc.h>

bool utils::parameter::getValueByName(const STDStringMultiMap& opts, const std::string& name, std::string* value)
{
    bool res(true);
    STDStringMultiMap::const_iterator it = opts.lower_bound(name);//игнорируем повторяющиеся, в данном reader-е они нам не нужны
    res = res && (it!=opts.end());
    if(res){
	utils::misc::fill(value, (*it).second);
    }
    return res;
}
