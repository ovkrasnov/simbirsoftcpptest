#ifndef DEFAULTREADER_H
#define DEFAULTREADER_H

#include <abstracts/abstractreader.h>
#include <fstream>
#include <locale>

class DefaultReader: public AbstractReader{
public:
    //TODO!!! описание
    DefaultReader();//+
    //TODO!!! описание
    ~DefaultReader();//+

    //TODO!!! описание
    bool isValid()const;//+
    static bool isValid(const std::string& filename);//+

    //TODO!!! описание
    void setInputLocale(const std::locale& locale);
    //TODO!!! описание
    std::string usage()const;//+
    //TODO!!! описание
    bool        read(std::wstring& buff, std::string* errstr=0);//+
    //TODO!!! описание
    bool loadOptions(const STDStringMultiMap& opts, std::string* errstr=0);//+

    //TODO!!! описание
    static size_t maxFileSizeInBytes();//+

    //TODO!!! описание
    std::string fileName()const;//+
    
    //TODO!!! описание
    std::locale locale()const;

protected:
    //TODO!!! описание
    bool init(const std::string& filename, const std::string& localename, std::string* errstr=0);//+
    //TODO!!! описание
    bool free(std::string* errstr=0);//+
private:
    std::string     m_filename;
    std::locale     m_locale;
    //int             m_maxlines;
    std::wifstream   m_filestream;
};

#endif//DEFAULTREADER_H
