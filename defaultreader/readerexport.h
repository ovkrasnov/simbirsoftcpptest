#ifndef READEREXPORT_H
#define READEREXPORT_H

#include <export_import.h>

# ifdef READERDEFAULT_LIB
#  define READERDEFAULTLIB_EXPORT DECL_EXPORT
# else
#  define READERDEFAULTLIB_EXPORT DECL_IMPORT
# endif

# ifdef __cplusplus
extern "C" {
# endif

READERDEFAULTLIB_EXPORT bool initReader(void** reader);
READERDEFAULTLIB_EXPORT bool freeReader(void** reader);

# ifdef __cplusplus
}
# endif

#endif//READEREXPORT_H
