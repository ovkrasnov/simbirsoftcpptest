#include "readerexport.h"
#include "defaultreader.h"


bool initReader(void** reader)
{
    bool res(true);
    res = res && reader;
    res = res && !(*reader);
    if(res){
        *reader = new DefaultReader;
    }
    return res;
}

bool freeReader(void **reader)
{
    bool res(true);
    res = res && reader;
    res = res && (*reader);
    if(res){
        delete (DefaultReader*)(*reader);
        *reader = 0;
    }
    return res;
}

