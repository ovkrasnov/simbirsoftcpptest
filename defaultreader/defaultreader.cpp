#include "defaultreader.h"
#include <utils/fs.h>
#include <utils/string.h>
#include <utils/wstring.h>
#include <utils/misc.h>
#include <utils/parameter.h>
#include <iostream>
#include <globals.h>
/*
static const int MIN_LINES_BOUND = 10;
static const int MAX_LINES_BOUND = 100000;
*/
//TODO!!! описание
DefaultReader::DefaultReader():
    AbstractReader(),
    m_filename(),
    m_locale(),
    //m_maxlines(10),
    m_filestream()
{}

//TODO!!! описание
DefaultReader::~DefaultReader()
{
    free();
}

//TODO!!! описание
bool DefaultReader::isValid()const
{
    return isValid(fileName());
}

bool DefaultReader::isValid(const std::string& filename){
    return utils::fs::isFileExists(filename);
}

//TODO!!! описание
std::string DefaultReader::usage()const
{
    static const std::string res(
	"To define library-specific options library \"defaultreader\"\n"
	"uses parameter pairs:\n"
	"\"filename=path_to_file\" - path_to_file specifies the path to file.\n"
	"\"locale=locale_name\" - locale_name specifies the name of locale which uses to read file.\n"
	//"\"maxlines=max_line_count\" - max_line_count specifies max lines to read.\n"
    );
    return res;
}

//TODO!!! описание
bool DefaultReader::read(std::wstring& buff, std::string *errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(isValid(), errstr, FUNC_INFO("Can't read the file because the reader is not valid!"));
    try{
	std::wstring str_buff;
	res = res && std::getline(m_filestream, buff);
	//std::cout << m_filestream.getloc().name();
	//std::cout << locale().name() << std::endl;
	//std::cout << std::wcout.getloc().name() << std::endl;
	//std::wcout.imbue(std::locale("C.UTF-8"));
	//std::wcout << str_buff << std::endl;
	//for(it = str_buff.begin(); it != str_buff.end(); ++it){
	//    std::wcout << *it;
	//}
	//res = res && utils::wstring::fillWString(&buff, str_buff, locale());
    }catch(std::exception& e){
	res = false;
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
    }
    return res;
}

//TODO!!! описание
bool DefaultReader::loadOptions(const STDStringMultiMap& opts, std::string *errstr)
{
    bool res(true);
    std::string filename;
    std::string localename;
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "filename", &filename),
	errstr,
	FUNC_INFO("Parameter 'filename' is not specified!")
    );
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "locale", &localename),
	errstr,
	FUNC_INFO("Parameter 'locale' is not specified!")
    );
    res = res && init(filename, localename, errstr);
    return res;
}

bool DefaultReader::init(const std::string& filename, const std::string& localename, std::string* errstr)
{
    bool res(true);
    try{
	res = res && utils::misc::ifFalseFill(
	    isValid(filename),//существует ли файл
	    errstr,
	    FUNC_INFO(std::string() + "File '" + filename += "' is not valid!")
	);
	res = res && utils::misc::ifFalseFill(
	    utils::fs::fileSizeInBytes(filename) < maxFileSizeInBytes(),
	    errstr,
	    FUNC_INFO(std::string() + "File '" + filename + "' has invalid size!")
	);
	if(res){
	    free();//на всякий случай
	    m_locale	= std::locale(localename.c_str());
	    m_filename	= filename;
	    m_filestream.imbue(m_locale);
	    m_filestream.open(filename.c_str());
	    //m_filestream.ignore();
	}
    }catch(std::exception& e){
	res = false;
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
	free();
    }
    return res;
}

//TODO!!! описание
bool DefaultReader::free(std::string *errstr)
{
    bool res(true);
    try{
	//возвращаем значения по-умолчанию
	m_filestream.close();
	m_filename	= "";
	m_locale	= std::locale("");
    }catch(std::exception& e){
	res = false;
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
    }
    return res;
}

//TODO!!! описание
std::string DefaultReader::fileName()const
{
    return m_filename;
}

//TODO!!! описание
size_t DefaultReader::maxFileSizeInBytes(){
    return utils::fs::MaxFileSizeInBytes;
}

std::locale DefaultReader::locale()const
{
    return m_locale;
}
