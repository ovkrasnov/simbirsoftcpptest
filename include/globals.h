#ifndef GLOBALS_H
#define GLOBALS_H

#include <utils/string.h>

#ifdef USE_DEBUG_INFO
# define FUNC_INFO(x) (std::string(__PRETTY_FUNCTION__) + ":(line=["+utils::string::toStdString(__LINE__)+"]): " + (x))
#else
# define FUNC_INFO(x) (std::string()+(x))
#endif

#endif//GLOBALS_H
