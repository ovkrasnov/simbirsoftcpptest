#ifndef TYPEDEFS_H
#define TYPEDEFS_H

#include <string>
#include <map>
#include <set>

typedef std::multimap<std::string, std::string> STDStringMultiMap;
typedef std::pair<std::string, std::string> STDStringPair;
typedef std::set<std::string> STDStringSet;

#endif//TYPEDEFS_H
