#ifndef ABSTRACTREADER_H
#define ABSTRACTREADER_H

#include <typedefs.h>
#include <utils/string.h>

//TODO!!! описание
class AbstractReader{
public:
    //TODO!!! описание
    AbstractReader(){}
    //TODO!!! описание
    virtual ~AbstractReader(){}

    //TODO!!! описание
    virtual bool isValid()const{return false;}

    //TODO!!! описание
    //virtual std::string className(  )const  =0;
    //TODO!!! описание
    virtual std::string usage(                              )const  =0;
    //TODO!!! описание
    virtual bool        read(std::wstring& buff, std::string* errstr=0)       =0;
    //TODO!!! описание
    virtual bool loadOptions(const STDStringMultiMap& opts, std::string* errstr=0)       =0;
};

#endif//ABSTRACTREADER_H
