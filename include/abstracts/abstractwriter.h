#ifndef ABSTRACTWRITER_H
#define ABSTRACTWRITER_H

#include <typedefs.h>

//TODO!!! описание
class AbstractWriter{
public:
    //TODO!!! описание
    AbstractWriter(){}
    //TODO!!! описание
    virtual ~AbstractWriter(){}

    //TODO!!! описание
    virtual bool isValid()const{return false;}

    //TODO!!! описание
    virtual std::string usage(                              )const  =0;
    //TODO!!! описание
    virtual bool write(const std::wstring& buff, std::string* errstr=0 )       =0;
    //TODO!!! описание
    virtual bool flush(std::string* errstr=0 )       =0;
    //TODO!!! описание
    virtual bool loadOptions(const STDStringMultiMap& opts, std::string* errstr=0  )       =0;
};

#endif//ABSTRACTWRITER_H
