#ifndef DEFINES_H
#define DEFINES_H

# ifdef OS_WIN
#  define DECL_EXPORT __declspec(dllexport)
#  define DECL_IMPORT __declspec(dllimport)
# else
#  ifdef OS_UNIX
#   define DECL_EXPORT
#   define DECL_IMPORT
#  endif
# endif

#endif
