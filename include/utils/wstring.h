#ifndef UTILS_WSTRING_H
#define UTILS_WSTRING_H

#include <string>
#include <sstream>

namespace utils{
    namespace wstring{
	//template<class T>
        //std::wstring toStdWString(const T& value)
	//{
	//    std::wstringstream ss;
	//    ss << value;
	//    return ss.str();
	//}
	//template<class T>
	//std::string toStdString(const T& value)
	//{
	//    std::stringstream ss;
	//    ss << value;
	//    return ss.str();
	//}
        //extern int toInt(const std::wstring& value, bool* ok=0);
        extern bool isNotWordDelimiter(wchar_t c);
        extern bool isWordDelimiter(wchar_t c);
        extern bool isNotSentenceDelimiter(wchar_t c);
        extern bool isSentenceDelimiter(wchar_t c);
        extern bool fillWString(std::wstring* dst, const std::string& src, const std::locale& src_locale);
	extern bool fillString(std::string* dst, const std::wstring& src, const std::locale& dst_locale);
        //extern bool replace(std::wstring* str, const std::wstring& what, const std::wstring& with);
        //extern const std::wstring Delimiters;
        extern bool replace(std::wstring* str, const std::wstring& what, const std::wstring& with);
    }//namespace wstring
}//namespace utils

#endif//UTILS_WSTRING_H
