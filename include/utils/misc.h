#ifndef UTILS_MISC_H
#define UTILS_MISC_H
namespace utils{
    namespace misc{
        template<class T>
        bool fill(T* dst, const T& src)
        {
            bool res(dst);
            if(res){
                *dst = src;
            }
            return res;
        }//bool utils::misc::fill(T* dst, const T& src)		template<class T>
        template<class T>
        bool ifFalseFill(bool v, T* dst, const T& src)
        {
            if(!v){
                fill(dst, src);
            }
            return v;
        }//bool utils::misc::ifFalseFill(bool v, T* dst, const T& src)
    }//namespace misc
}//namespace utils
#endif//UTILS_MISC_H
