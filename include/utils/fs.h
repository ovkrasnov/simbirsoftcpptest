#ifndef UTILS_FS_H
#define UTILS_FS_H

#include <string>
#include <stdexcept>

namespace utils{
	namespace fs{
		extern const size_t MaxFileSizeInBytes;//максимальный размерфайла в байтах
		//TODO!!! описание
		extern bool isFileExists(const std::string& filename);
        //extern bool isFileNameCorrect(const std::string& filename);
		//TODO!!! описание
		extern size_t fileSizeInBytes(const std::string& filename) throw(std::runtime_error);
		//TODO!!! описание
		extern std::string generateNumberedFileName(const std::string& prefix, size_t& currn, size_t stopn, size_t nstrlen, const std::string& suffix)throw(std::runtime_error);
		}//namespace fs
}//namespace utils

#endif//UTILS_FS_H
