#ifndef UTILS_PARAMETER_H
#define UTILS_PARAMETER_H

#include <typedefs.h>

namespace utils{
    namespace parameter{
	bool getValueByName(const STDStringMultiMap& opts, const std::string& name, std::string* value);
    }//namespace parameter}
}//namespace utils
#endif//UTILS_PARAMETER_H
