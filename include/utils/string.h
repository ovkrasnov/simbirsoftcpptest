#ifndef UTILS_STRING_H
#define UTILS_STRING_H

#include <string>
#include <sstream>

namespace utils{
    namespace string{
	template<class T>
	std::string toStdString(const T& value)
	{
	    std::stringstream ss;
	    ss << value;
	    return ss.str();
	}
        extern int toInt(const std::string& value, bool* ok=0);
    }//namespace string
}//namespace utils

#endif//UTILS_STRING_H
