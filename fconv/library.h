#ifndef LIBRARY_H
#define LIBRARY_H

#include <string>

/*
    TODO!!! описание
*/
class Library{
public:
    //TODO!!! описание
    Library(const std::string& assembly=std::string());
    //TODO!!! описание
    ~Library();
    
    //TODO!!! описание
    bool        isValid()const;
    //TODO!!! описание
    std::string lastErrorText()const;
    
    //TODO!!! описание
    bool    load(const std::string& assembly);
    //TODO!!! описание
    bool    unload();
    //TODO!!! описание
    void*   resolve(const std::string& symbol);

protected:
    void        setLastErrorText(const std::string& v);
    void*       library();
    const void* constLibrary()const;
private:
    void*       m_library;
    std::string m_last_error_text;
};

#endif//LIBRARY_H
