#ifndef RWFACTORY_H
#define RWFACTORY_H

#include <typedefs.h>
#include "library.h"
#include <abstracts/abstractreader.h>
#include <abstracts/abstractwriter.h>


class RWFactory{
public:
    //TODO!!! описание
    RWFactory(
    );
    //TODO!!! описание
    ~RWFactory();

    //TODO!!! описание
    bool init(const std::string& rassembly, const std::string& wassembly, std::string *errstr=0);
    //TODO!!! описание
    void free();

    //TODO!!! описание
    bool isValid()const;

    //TODO!!! описание не забыть сказать, что ридера надо удалять самому
    AbstractReader* createReader(
	const STDStringMultiMap& options = STDStringMultiMap(),
	std::string* errstr = 0
    );
    //TODO!!! описание не забыть сказать, что райтера надо удалять самому
    AbstractWriter* createWriter(
	const STDStringMultiMap& options = STDStringMultiMap(),
	std::string* errstr = 0
    );
    //TODO!!! описание
    bool            destroyReader(AbstractReader** reader, std::string *errstr = 0);
    //TODO!!! описание
    bool            destroyWriter(AbstractWriter** writer, std::string *errstr = 0);
protected:
    //TODO!!! описание
    Library*        readerLibrary();
    //TODO!!! описание
    Library*        writerLibrary();
    //TODO!!! описание
    const Library*  constReaderLibrary()const;
    //TODO!!! описание
    const Library*  constWriterLibrary()const;
private:
    Library*     m_reader_library    ;
    Library*     m_writer_library    ;
};

#endif//RWFACTORY_H

