#include "rwfactory.h"
#include <iostream>
#include <utils/string.h>
#include <utils/misc.h>
#include <globals.h>
#include <stdio.h>

typedef bool(P2Init)(void**);
typedef bool(P2Free)(void**);

RWFactory::RWFactory(
//    const std::string& rassembly,
//    const std::string& wassembly
):
    m_reader_library(0),
    m_writer_library(0)
{
    //init(rassembly, wassembly);
}

RWFactory::~RWFactory()
{
    free();
}

bool RWFactory::isValid()const
{
    bool res(true);
    res = res && constReaderLibrary();
    res = res && constWriterLibrary();
    res = res && constReaderLibrary()->isValid();
    res = res && constWriterLibrary()->isValid();
    return res;
}

AbstractReader* RWFactory::createReader(const STDStringMultiMap& options, std::string *errstr)
{
    AbstractReader* res=0;
    bool ok(true);
    try{
	ok = ok && utils::misc::ifFalseFill(constReaderLibrary(), errstr, FUNC_INFO("Reader library was not loaded!"));
	ok = ok && utils::misc::ifFalseFill(constReaderLibrary()->isValid(), errstr, FUNC_INFO("Reader library is not valid!"));
	if(ok){
	    P2Init* p2init = (P2Init*)(readerLibrary()->resolve("initReader"));
	    ok = ok && utils::misc::ifFalseFill(p2init, errstr, FUNC_INFO("Can't resolve initReader function!"));
	    ok = ok && utils::misc::ifFalseFill(p2init((void**)&res), errstr, FUNC_INFO("Can't init reader!"));
	    ok = ok && utils::misc::ifFalseFill(res, errstr, FUNC_INFO("Can't create reader descriptor!"));
	    if(ok){
		ok = ok && res->loadOptions(options, errstr);
		if(!ok){
		    destroyReader(&res);
		}
	    }
	}
    }catch(std::exception& e){
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
	destroyReader(&res);
    }
    return res;
}

AbstractWriter* RWFactory::createWriter(const STDStringMultiMap& options, std::string *errstr)
{
    AbstractWriter* res=0;
    bool ok(true);
    try{
	ok = ok && utils::misc::ifFalseFill(constWriterLibrary(), errstr, FUNC_INFO("Writer library was not loaded!"));
	ok = ok && utils::misc::ifFalseFill(constWriterLibrary()->isValid(), errstr, FUNC_INFO("Writer library is not valid!"));
	if(ok){
	    P2Init* p2init = (P2Init*)(writerLibrary()->resolve("initWriter"));
	    ok = ok && utils::misc::ifFalseFill(p2init, errstr, FUNC_INFO("Can't resolve initWriter function!"));
	    ok = ok && utils::misc::ifFalseFill(p2init((void**)&res), errstr, FUNC_INFO("Can't init writer!"));
	    ok = ok && utils::misc::ifFalseFill(res, errstr, FUNC_INFO("Can't create writer descriptor!"));
	    if(ok){
		ok = ok && res->loadOptions(options, errstr);
		if(!ok){
		    destroyWriter(&res);
		}
	    }
	}
    }catch(std::exception& e){
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
	destroyWriter(&res);
    }
    return res;
}

bool RWFactory::destroyReader(AbstractReader** reader, std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(reader, errstr, FUNC_INFO("The pointer to reader descriptor is null!"));
    res = res && utils::misc::ifFalseFill(*reader, errstr, FUNC_INFO("The reader descriptor is null!"));
    res = res && utils::misc::ifFalseFill(constReaderLibrary(), errstr, FUNC_INFO("The reader library descriptor is null!"));
    if(res){
	P2Free* p2free = (P2Free*)(readerLibrary()->resolve("freeReader"));
	res = res && utils::misc::ifFalseFill(p2free, errstr, FUNC_INFO("Can't resolve freeReader function!"));
	res = res && utils::misc::ifFalseFill(p2free((void**)reader), errstr, FUNC_INFO("Can't free the reader!"));
	if(res){
	    *reader=0;//на всякий случай
	}
    }
    return res;
}

bool RWFactory::destroyWriter(AbstractWriter** writer, std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(writer, errstr, FUNC_INFO("The pointer to writer descriptor is null!"));
    res = res && utils::misc::ifFalseFill(*writer, errstr, FUNC_INFO("The writer descriptor is null!"));
    res = res && utils::misc::ifFalseFill(constWriterLibrary(), errstr, FUNC_INFO("The writer library descriptor is null!"));
    if(res){
	P2Free* p2free = (P2Free*)(writerLibrary()->resolve("freeWriter"));
	res = res && utils::misc::ifFalseFill(p2free, errstr, FUNC_INFO("Can't resolve freeWriter function!"));
	res = res && utils::misc::ifFalseFill(p2free((void**)writer), errstr, FUNC_INFO("Can't free the writer!"));
	if(res){
	    *writer=0;//на всякий случай
	}
    }
    return res;
}

Library* RWFactory::readerLibrary()
{
    return m_reader_library;
}

Library* RWFactory::writerLibrary()
{
    return m_writer_library;
}

const Library* RWFactory::constReaderLibrary()const
{
    return m_reader_library;
}

const Library* RWFactory::constWriterLibrary()const
{
    return m_writer_library;
}

bool RWFactory::init(const std::string& rassembly, const std::string& wassembly, std::string* errstr)
{
    bool res(true);
    free();//на всякий случай
    
    if(res){
	m_reader_library = new Library(rassembly);
	res = res && utils::misc::ifFalseFill(
	    constReaderLibrary(),
	    errstr,
	    FUNC_INFO(rassembly + " not loaded!")
	);
	res = res && utils::misc::ifFalseFill(
	    constReaderLibrary()->isValid(),
	    errstr,
	    FUNC_INFO(rassembly + " is not valid(" + constReaderLibrary()->lastErrorText() + ")!")
	);
	if(res){
	    m_writer_library = new Library(wassembly);
	    res = res && utils::misc::ifFalseFill(
		constWriterLibrary(),
		errstr,
		FUNC_INFO(wassembly + " not loaded!")
	    );
	    res = res && utils::misc::ifFalseFill(
		constWriterLibrary()->isValid(),
		errstr,
		FUNC_INFO(wassembly + " is not valid(" + constWriterLibrary()->lastErrorText() + ")!")
	    );
	}
    }
    return res;
}

void RWFactory::free()
{
    if(m_reader_library){
	delete m_reader_library;
	m_reader_library = 0;
    }
    if(m_writer_library){
	delete m_writer_library;
	m_writer_library = 0;
    }
}
