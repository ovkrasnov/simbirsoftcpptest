#include <iostream>
#include "options.h"
#include <stdio.h>
#include "rwfactory.h"
#include "converter.h"
#include <utils/wstring.h>
#include <utils/misc.h>
#include <globals.h>
#include <locale>

int main(int argc, char **argv)
{
    bool ok(true);
    std::string err_text;
    try{
	//устанавливаем в качестве внутренней локали C.UTF8
	std::cout << "Trying to set 'C.UTF8' locale as internal locale...";
	std::locale::global(std::locale("C.UTF-8"));
	std::cout << "   [ok!]\n";
    }catch(std::exception& e){
	std::cout << "   [error].\n";
	err_text = "The locale named 'C.UTF-8' is not supported!";
	ok = false;
    }
    ok = ok && Options::load(argc, argv, &err_text);
    ok = ok && utils::misc::ifFalseFill(Options::isValid(), &err_text, std::string("Options is not valid!"));
    if(ok){
	if(!Options::needHelp()){
	    RWFactory rwf;
	    ok = ok && rwf.init(Options::readerAssembly(), Options::writerAssembly(), &err_text);
	    if(ok){
		AbstractReader *reader = rwf.createReader(Options::readerSpecialOptions(), &err_text);
		ok = ok && reader;
		if(ok){
		    AbstractWriter *writer = rwf.createWriter(Options::writerSpecialOptions(), &err_text);
		    ok = ok && writer;
		    if(ok){
			Converter converter;
			converter.setReader(reader);
			converter.setWriter(writer);
			ok = ok && converter.convert(&err_text);
			if(ok){
			    std::cout << "The conversion was successfull!\n";
			}else{
			    std::cout << reader->usage() << "\n";
			    std::cout << writer->usage() << "\n";
			}
		    }
		    rwf.destroyWriter(&writer);
		}
		rwf.destroyReader(&reader);
	    }
	}else{
	    std::cout << Options::usage(argc>0?argv[0]:"fconv");
	}
    }
    if(!ok){
	std::cerr << err_text << "\n";
    }
    return 0;
}

