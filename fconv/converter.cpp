#include "converter.h"
#include "library.h"
#include <iostream>
#include <globals.h>
#include <stdio.h>
#include <utils/misc.h>

//TODO!!! описание
Converter::Converter():
    m_reader(0),
    m_writer(0)
{}

//TODO!!! описание
Converter::~Converter()
{}


//TODO!!! описание
bool Converter::isValid()const
{
    bool res(true);
    res = res && constReader();
    res = res && constWriter();
    res = res && constReader()->isValid();
    res = res && constWriter()->isValid();
    return res;
}

//TODO!!! описание
AbstractReader* Converter::reader()
{
    return m_reader;
}

//TODO!!! описание
AbstractWriter* Converter::writer()
{
    return m_writer;
}

//TODO!!! описание
const AbstractReader* Converter::constReader()const
{
    return m_reader;
}

//TODO!!! описание
const AbstractWriter* Converter::constWriter()const
{
    return m_writer;
}

//TODO!!! описание
bool Converter::convert(std::string *errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(isValid(), errstr, FUNC_INFO("The converter is not valid!"));
    if(res){
	std::wstring buff;
	try{
	    while(res && reader()->read(buff)){
		res = res && writer()->write(buff, errstr);
	    }
	    if(res){
		writer()->flush();
	    }
	}catch(std::exception& e){
	    res = false;
	    utils::misc::fill(errstr, FUNC_INFO(e.what()));
	}
    }
    return res;
}

//TODO!!! описание
void Converter::setReader(
    AbstractReader* v
)
{
    m_reader = v;
}

//TODO!!! описание
void Converter::setWriter(
    AbstractWriter *v
)
{
    m_writer = v;
}

