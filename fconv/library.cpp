#include "library.h"
#include <utils/string.h>
#include <iostream>

#ifdef OS_WIN
# include <windows.h>
#else
# ifdef OS_UNIX
#  include <dlfcn.h>
# endif
#endif

//TODO!!! описание
Library::Library(const std::string& assembly):
    m_library(0),
    m_last_error_text()
{
    load(assembly);
}

//TODO!!! описание
Library::~Library()
{
    unload();
}
    
//TODO!!! описание
bool Library::isValid()const
{
    return constLibrary();
}

//TODO!!! описание
std::string Library::lastErrorText()const
{
    return m_last_error_text;
}

//TODO!!! описание
void Library::setLastErrorText(const std::string &v)
{
    m_last_error_text = v;
}

//TODO!!! описание
bool Library::load(const std::string& assembly)
{
    unload();
#ifdef OS_WIN
    HINSTANCE hinstance = LoadLibraryA(assembly.c_str());
    if(!hinstance){
	hinstance = LoadLibraryA(std::string(assembly).c_str());
	if(!hinstance){
	    setLastErrorText(
		std::string("Library::load: Can't load library ")
		+ assembly
		+ ": code="
		+ utils::string::toStdString(int(GetLastError()))
		+ "\n"
	    );
	}
    }
    if(hinstance){
	m_library = (void*)hinstance;
    }
#else
# ifdef OS_UNIX
    m_library = dlopen(assembly.c_str(), RTLD_LAZY);
    //TODO!!! можно использовать dlerror для формирования строки ошибки
    if(!m_library){
	m_library = dlopen(std::string(assembly).c_str(), RTLD_LAZY);
	if(!m_library){
	    setLastErrorText(
		std::string("Library::load: Can't load library ")
		+ assembly
		+ ": "
		+ dlerror()
		+ "\n"
	    );
	}
    }
# else
    return false;
# endif
#endif
/*    if(!isValid()){
        setLastErrorText(
            std::string("An error occured while loading library ")
            + assembly
        );
    }
*/    return isValid();
}

//TODO!!! описание
void* Library::resolve(const std::string& symbol)
{
    void* res = 0;
    if(isValid()){
#ifdef OS_WIN
	res = (void*)GetProcAddress((HINSTANCE)library(), LPCSTR(symbol.c_str()));
#else
# ifdef OS_UNIX
	res = dlsym(library(), symbol.c_str());
	//TODO!!! можно использовать dlerror для формирования строки ошибки
# endif
#endif
    }
    if(!res){
	setLastErrorText(
	    std::string("An error occured while loading symbol ")
	    + symbol
	);
    }
    return res;
}

//TODO!!! описание
bool Library::unload()
{
    bool res(true);
    if(isValid()){
#ifdef OS_WIN
	res = FreeLibrary((HINSTANCE)library());
#else
# ifdef OS_UNIX
	res = (!dlclose(m_library));
	//TODO!!! можно использовать dlerror для формирования строки ошибки
# endif
#endif
    }
    if(res){
	m_library = 0;
    }else{
	setLastErrorText("An error occured while unloading library");
    }
    return res;
}

const void* Library::constLibrary()const
{
    return m_library;
}

void* Library::library()
{
    return m_library;
}
