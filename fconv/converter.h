#ifndef CONVERTER_H
#define CONVERTER_H

#include <abstracts/abstractreader.h>
#include <abstracts/abstractwriter.h>

/*
  TODO!!! описание
*/
class Converter{
public:
    //TODO!!! описание
    Converter();
    //TODO!!! описание
    ~Converter();

    //TODO!!! описание
    bool isValid()const;

    //TODO!!! описание
    AbstractReader*         reader();
    //TODO!!! описание
    AbstractWriter*         writer();
    //TODO!!! описание
    const AbstractReader*   constReader()const;
    //TODO!!! описание
    const AbstractWriter*   constWriter()const;

    //TODO!!! описание
    void setReader(
        AbstractReader* v
    );
    //TODO!!! описание
    void setWriter(
        AbstractWriter* v
    );

    //TODO!!! описание
    bool convert(std::string* errstr);

private:
    AbstractReader* m_reader;
    AbstractWriter* m_writer;
};

#endif//CONVERTER_H
