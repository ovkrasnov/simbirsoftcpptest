#include "options.h"
#include <utils/string.h>
#include <utils/misc.h>
#include <unistd.h>
#include <globals.h>
#include <stdio.h>
#include <iostream>

//Options* Options::m_self = 0;
std::string Options::m_reader_assembly(defaultReaderAssembly());
std::string Options::m_writer_assembly(defaultWriterAssembly());
STDStringMultiMap Options::m_reader_special_options;
STDStringMultiMap Options::m_writer_special_options;
bool Options::m_need_help = false;
//TODO!!! описание
Options::Options()
{}

//TODO!!! описание
Options::~Options()
{}

/*
//TODO!!! описание
Options* Options::instance()
{
    return m_self;
}
*/

//TODO!!! описание
typedef enum{NotFound, Found, IncorrectArgCount} ArgvParseResult;
ArgvParseResult tryParseArgvPairs();

bool Options::load(int argc, char** argv, std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(
	isValid(argc, argv),
	errstr,
	FUNC_INFO("Command line arguments is not valid!")
    );
    if(res){
	clear();
	int presult(0);
	m_need_help = false;
	while(res && (presult=getopt(argc,argv,"hr:w:s:x:"))!=-1){
	    switch(presult){
	    case 'h':
		m_need_help = true;
		return res;
		break;
	    case 'r':
		res = res && utils::misc::ifFalseFill(optarg, errstr, FUNC_INFO("Reader is not specified after -r!"));
		if(res){
		    setReaderAssembly(optarg);
		}
		break;
	    case 'w':
		res = res && utils::misc::ifFalseFill(optarg, errstr, FUNC_INFO("Writer is not specified after -w!"));
		if(res){
		    setWriterAssembly(optarg);
		}
		break;
	    case 's':
		res = res && trySetSpecialOption(optarg, &m_reader_special_options, errstr);
		break;
	    case 'x':
		res = res && trySetSpecialOption(optarg, &m_writer_special_options, errstr);
		break;
	    case '?':
	    default:
		res = false;
		break;
	    }
	}
    }
    return res;
}

//TODO!!! описание
bool Options::isValid()
{
    bool res(true);
    res = res && !readerAssembly().empty();
    res = res && !writerAssembly().empty();
    return res;
}

//TODO!!! описание
bool Options::isValid(int argc, char** argv)
{
    bool res(true);
    res = res && argc>0;
    res = res && argv;
    return res;
}

//TODO!!! описание
std::string Options::readerAssembly()
{
    return m_reader_assembly;
}

//TODO!!! описание
std::string Options::writerAssembly()
{
    return m_writer_assembly;
}

//TODO!!! описание
void Options::setReaderAssembly(const std::string& v)
{
    m_reader_assembly = v;
}

//TODO!!! описание
void Options::setWriterAssembly(const std::string& v)
{
    m_writer_assembly = v;
}

//TODO!!! описание
STDStringMultiMap Options::readerSpecialOptions()
{
    return m_reader_special_options;
}

//TODO!!! описание
STDStringMultiMap Options::writerSpecialOptions()
{
    return m_writer_special_options;
}

//TODO!!! описание
void Options::setReaderSpecialOptions(const STDStringMultiMap& v)
{
    m_reader_special_options = v;
}

//TODO!!! описание
void Options::setWriterSpecialOptions(const STDStringMultiMap& v)
{
    m_writer_special_options = v;
}

//TODO!!! описание
void Options::addReaderSpecialOption(
    const std::string& key,
    const std::string& value
)
{
    m_reader_special_options.insert(std::make_pair(key,value));
}

//TODO!!! описание
void Options::addWriterSpecialOption(
    const std::string& key,
    const std::string& value
)
{
    m_writer_special_options.insert(std::make_pair(key,value));
}

//TODO!!! описание
bool Options::removeReaderSpecialOption(const std::string& key, std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(
	m_reader_special_options.erase(key)>0,
	errstr,
	FUNC_INFO("Can't find key \""+key+"\"!")
    );
    return res;
}
//TODO!!! описание
bool Options::removeWriterSpecialOption(const std::string& key, std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(
	m_writer_special_options.erase(key)>0,
	errstr,
	FUNC_INFO("Can't find key \""+key+"\"!")
    );
    return res;
}

std::string Options::usage(const std::string& appname)
{
    std::string res("Usage: ");
    res +=  appname;
    res +=  " [ReadOptions] [WriteOptions]\n";
    res +=
	    "\n"
	    "This application is designed for converting the input file\n"
	    "to the output file in accordance with the user-specified\n"
	    "rules.\n"
	    "\n"
	    "  -h -\n"
	    "      Shows this help.\n"
	    "\n"
	    "ReadOptions:\n"
	    "  -r -\n"
	    "      (multiplicity: [0..1])\n"
	    "      Library name, meant for reading an input file. If this\n"
	    "      parameter is not specified, then default value\n"
	    "      \"";
    res +=  defaultReaderAssembly();
    res +=
	    "\" will be used.\n";
    res +=
	    "  -s -\n"
	    "      (multiplicity: [0..n])\n"
	    "      Input file reader specific option represented by key=value\n"
	    "      pair. For more information about reader-specific parameters\n"
	    "      see the reader's documentation.\n"
//	    "      The " + defaultReaderAssembly + "uses an additional\n"
//	    "      parameter called 'locale', if not specified will use\n"
//	    "      the system locale"
	    "\n"
	    "WriteOptions:\n"
	    "  -w -\n"
	    "      (multiplicity: [0..1])\n"
	    "      Library name, meant for writing an output file. If this\n"
	    "      parameter is not specified, then default value is used\n"
	    "      \"";
    res +=  defaultWriterAssembly();
    res +=
	    "\" will be used.\n"
	    "  -x -\n"
	    "      (multiplicity: [0..n])\n"
	    "      Output file writer specific option represented by key=value\n"
	    "      pair. For more information about writer-specific parameters\n"
	    "      see the writer's documentation.\n"
	    "\n";
    return res;
}

//TODO!!! описание
std::string Options::defaultReaderAssembly()
{
    std::string res;
#ifdef OS_WIN
    res = "defaultreader.dll";
#else
    res = "libdefaultreader.so";
#endif
    return res;
}
//TODO!!! описание
std::string Options::defaultWriterAssembly()
{
    std::string res;
#ifdef OS_WIN
    res = "defaultwriter.dll";
#else
    res = "libdefaultwriter.so";
#endif
    return res;
}
//TODO!!! описание
void Options::clear()
{
    m_reader_assembly = defaultReaderAssembly();
    m_writer_assembly = defaultWriterAssembly();
    m_reader_special_options.clear();
    m_writer_special_options.clear();
}

//TODO!!! описание
bool Options::trySetSpecialOption(
    const std::string& keyvalue,
    STDStringMultiMap* destination,
    std::string* errstr
)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(destination, errstr, FUNC_INFO("Can't write to null pointed destination!"));
    if(res){
	size_t index = keyvalue.find('=');
	//res = res && (index != std::string::npos);//не нужно, т.к. дальнейшие сравнения покрывают это условие
	res = res && utils::misc::ifFalseFill(
	    (index > 0) && (index < keyvalue.length()-1),
	    errstr,
	    FUNC_INFO("Can't' parse special parameter string \""+keyvalue+"\"")
	);
	if(res){
	    /*
	    keyvalue=
	    "abc=defg"
	         ^
	    01234567
	    |=>
	    key   = str.substr(0, index);
	    value = str.substr(index+1);
	    */
	    destination->insert(
		STDStringPair(
		    keyvalue.substr(0, index),
		    keyvalue.substr(index + 1)
		)
	    );
	}
    }
    return res;
}

bool Options::needHelp()
{
    return m_need_help;
}
