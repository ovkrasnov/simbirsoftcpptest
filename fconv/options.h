//TODO!!! описание
#ifndef OPTIONS_H
#define OPTIONS_H

#include <typedefs.h>

class Options
{
public:
    //TODO!!! описание
    //static Options* instance();
    //TODO!!! описание 
    //Внимание!!! (для работы с параметрами запуска приложения используется системная локаль!)
    static bool load(int argc=0, char** argv=0, std::string* errstr=0);
    //TODO!!! описание
    static bool isValid();
    //TODO!!! описание
    static bool isValid(int argc, char**argv);

    //TODO!!! описание
    static std::string readerAssembly();
    //TODO!!!
    static std::string writerAssembly();

    //TODO!!! описание
    static void setReaderAssembly(const std::string& v);
    //TODO!!! описание
    static void setWriterAssembly(const std::string& v);

    //TODO!!! описание
    static STDStringMultiMap readerSpecialOptions();
    //TODO!!! описание
    static STDStringMultiMap writerSpecialOptions();
    //TODO!!! описание
    static void setReaderSpecialOptions(const STDStringMultiMap& v);
    //TODO!!! описание
    static void setWriterSpecialOptions(const STDStringMultiMap& v);
    //TODO!!! описание
    static void addReaderSpecialOption(
	const std::string& key,
	const std::string& value
    );
    //TODO!!! описание
    static void addWriterSpecialOption(
	const std::string& key,
	const std::string& value
    );
    //TODO!!! описание
    static bool removeReaderSpecialOption(const std::string& key, std::string* errstr=0);
    //TODO!!! описание
    static bool removeWriterSpecialOption(const std::string& key, std::string* errstr=0);
    //TODO!!! описание
    static std::string usage(const std::string &appname);


    //TODO!!! описание
    static std::string defaultReaderAssembly();
    //TODO!!! описание
    static std::string defaultWriterAssembly();
    //TODO!!! описание
    static bool needHelp();

protected:
    //TODO!!! описание
    Options();
    //TODO!!! описание
    ~Options();
    //TODO!!! описание
    static void clear();
    //TODO!!! описание
    static bool trySetSpecialOption(
	const std::string& keyvalue,
	STDStringMultiMap* destination,
	std::string* errstr=0
    );
private:
    //TODO!!! описание
    static Options* m_self;
    //TODO!!! описание
    static std::string m_reader_assembly;
    //TODO!!! описание
    static std::string m_writer_assembly;
    //TODO!!! описание
    static STDStringMultiMap m_reader_special_options;
    //TODO!!! описание
    static STDStringMultiMap m_writer_special_options;
    //TODO!!! описание
    static bool m_need_help;
};

#endif//OPTIONS_H
