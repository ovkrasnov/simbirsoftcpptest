OBJECTS_DIR = $$PWD/../_generated/$$TARGET/_obj
QT -= core gui
CONFIG(debug, debug|release){
    DESTDIR = $$PWD/../bin/debug
} else {
    DESTDIR = $$PWD/../bin/release
}

# Проект разработан для 2-х семейств ОС: unix и windows. Если проект собирается
# не в ОС семейства UNIX, считается, что он собирается в ОС семейства Windows
DEFINES         += OS_WIN
DEFINES		+= USE_DEBUG_INFO
unix{
    DEFINES     -= OS_WIN
    DEFINES     += OS_UNIX
}
