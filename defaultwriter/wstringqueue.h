#ifndef WSTRINGQUEUE_H
#define WSTRINGQUEUE_H

#include <queue>
#include <string>

class WStringQueue
{
    public:
	//TODO!!! описание
	WStringQueue();
	//TODO!!! описание
	WStringQueue(const WStringQueue& another);
	//TODO!!! описание
	WStringQueue& operator=(const WStringQueue& another);
	
	//TODO!!!описание
	bool operator==(const WStringQueue& another)const;
	//TODO!!! описание
	bool operator!=(const WStringQueue& another)const;

	//TODO!!! описание
	size_t size()const;
	//TODO!!! описание
	bool isEmpty()const;

	//TODO!!! описание
	virtual WStringQueue& join(const std::wstring& delimiter=L"");

	//TODO!!! описание
	virtual void clear();

	//TODO!!! описание
	virtual bool enqueue(const std::wstring& v);

	//TODO!!! описание
	virtual bool enqueue(const WStringQueue& v);

	//TODO!!! описание
	virtual std::wstring dequeue();
    private:
	std::queue<std::wstring> m_queue;
};

#endif//WSTRINGQUEUE_H
