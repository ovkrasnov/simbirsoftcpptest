#ifndef WRITEREXPORT_H
#define WRITEREXPORT_H

#include <export_import.h>

# ifdef WRITERDEFAULT_LIB
#  define WRITERDEFAULTLIB_EXPORT DECL_EXPORT
# else
#  define WRITERDEFAULTLIB_EXPORT DECL_IMPORT
# endif

# ifdef __cplusplus
extern "C" {
# endif

WRITERDEFAULTLIB_EXPORT bool initWriter(void** writer);
WRITERDEFAULTLIB_EXPORT bool freeWriter(void** writer);

# ifdef __cplusplus
}
# endif

#endif//WRITEREXPORT_H
