#ifndef DEFAULTWRITER_H
#define DEFAULTWRITER_H

#include <abstracts/abstractwriter.h>
#include <typedefs.h>
#include <vector>
#include <locale>
#include "bwstringqueue.h"

typedef std::vector<std::wstring> Dictionary;

class DefaultWriter: public AbstractWriter{
public:
    //TODO!!! описание
    DefaultWriter();//+
    //TODO!!! описание
    ~DefaultWriter();//+

    //TODO!!! описание
    bool isValid()const;//+

    //TODO!!! описание
    std::string usage()const;//+
    //TODO!!! описание предупредить, что в строке не должно быть "\n"
    bool write(const std::wstring& line, std::string* errstr=0);//+
    //TODO!!! описание
    bool flush(std::string* errstr=0);//+

    //TODO!!! описание
    bool loadOptions(const STDStringMultiMap& opts, std::string* errstr=0);

    //TODO!!! описание
    //size_t  maxLines() const;
    //TODO!!! описание
    //size_t  currentLine()const;
    //TODO!!! описание
    std::wstring currentWord()const;
    //TODO!!! описание
    BWStringQueue currentText()const;
    //TODO!!! описание
    WStringQueue currentSentence()const;

    //TODO!!! описание
    std::string outputFileName()const;

protected:
    //TODO!!! описание
    bool init(
	const std::string& output_filename,
	const std::string& dictionary_filename,
	const std::string& dictionary_localename,
	const std::string& maxlines,
	std::string *errstr
    );
    //TODO!!! описание
    bool tryInitDictionary(const std::string& filename, const std::string& localename, Dictionary* dst, std::string* errstr=0);
    //TODO!!! описание
    bool tryInitCurrentText(const std::string& maxlines, std::string* errstr=0);

    //TODO!!! описание
    void free();

    //TODO!!! описание
    bool flushCurrentSentence(std::string* errstr);
    //TODO!!! описание
    bool flushCurrentText(std::string* errstr);

    //TODO!!! описание
    static std::wstring htmlString(const std::wstring& src);
    //TODO!!! описание
    static std::wstring htmlString(wchar_t c);
    //TODO!!! описание
    //bool setCurrentLine(size_t v, std::string* errstr=0);
    //TODO!!! описание
    //bool incCurrentLine(std::string* errstr=0);
    //TODO!!! описание
    void resetCurrentText();
    //TODO!!! описание
    void resetCurrentSentence();
    //TODO!!! описание
    void resetCurrentWord();

    //TODO!!! описание, не забыть сказать, что текст должен быть переведен в html до вызова метода
    void append2CurrentSentence(const std::wstring& htmltext);
    //TODO!!! описание, не забыть сказать, что текст должен быть переведен в html до вызова метода
    void append2CurrentText(const std::wstring& htmltext);
    //TODO!!! описание, не забыть сказать, что текст должен быть переведен в html до вызова метода
    void append2CurrentWord(const std::wstring& htmltext);

private:
    //size_t			m_maxlines		;
    //size_t			m_currentline		;
    std::string			m_outputfilename	;
    Dictionary			m_dictionary		;
    std::wstring		m_currentword		;
    BWStringQueue		m_currenttext		;
    //std::wstring		m_currenttext		;
    WStringQueue		m_currentsentence	;
    //std::wstring		m_currentsentence	;
    //size_t			m_currentsentenceline	;
    size_t			m_currentfilenumber	;
};

#endif//DEFAULTWRITER_H
