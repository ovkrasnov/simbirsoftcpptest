#include "bwstringqueue.h"
#include <utils/misc.h>
#include <globals.h>
#include <iostream>

//TODO!!! описание
BWStringQueue::BWStringQueue(size_t upperbound)//:
    //m_upperbound(0)
{
    setUpperBound(upperbound);
}

//TODO!!! описание
BWStringQueue::BWStringQueue(const BWStringQueue& another):
    WStringQueue(another)
{
    setUpperBound(another.upperBound());
}

//TODO!!! описание
BWStringQueue& BWStringQueue::operator=(const BWStringQueue& another)
{
    if(this != &another){
	WStringQueue::operator=(another);
	setUpperBound(another.upperBound());
    }
    return *this;
}

//TODO!!!описание
bool BWStringQueue::operator==(const BWStringQueue& another)const
{
    bool res(true);
    res = res && WStringQueue::operator==(another);
    res = res && (upperBound() == another.upperBound());
    return res;
}

//TODO!!! описание
bool BWStringQueue::operator!=(const BWStringQueue& another)const
{
    return !operator==(another);
}

//TODO!!! описание
size_t BWStringQueue::upperBound()const
{
    return m_upperbound;
}

//TODO!!! описание
bool BWStringQueue::setUpperBound(size_t upperbound)
{
    bool res(true);
    std::string _errstr;
    res = res && utils::misc::ifFalseFill(upperbound>0, &_errstr, FUNC_INFO("Parameter 'upperbound' must be greater than 0!"));
    if(res){
	for(size_t i = upperbound + 1; i < size(); i++){
	    //приводим размер очереди в соответствие с новой границей, удаляя из нее лишние элементы
	    dequeue();
	}
	m_upperbound = upperbound;
    }
    if(!res){
	std::cout << _errstr << std::endl;
    }
    return res;
}

bool BWStringQueue::enqueue(const std::wstring& v)
{
    bool res(true);
    std::string _errstr;
    res = res && utils::misc::ifFalseFill(size()<upperBound(), &_errstr, FUNC_INFO("Can't enqueue new element due to the upper bound has been reached!"));
    res = res && utils::misc::ifFalseFill(WStringQueue::enqueue(v), &_errstr, FUNC_INFO("Can't enqueue new element due to an internal error!"));
    if(!res){
	std::cout << _errstr << std::endl;
    }
    return res;
}

//TODO!!! описание
bool BWStringQueue::enqueue(const WStringQueue& v)
{
    bool res(true);
    WStringQueue v_copy(v);
    std::string _errstr;
    res = res && utils::misc::ifFalseFill(size()+v_copy.size()<=upperBound(), &_errstr, FUNC_INFO("Can't enqueue another queue due to violation of upper bound!"));
    if(res){
	while(!v_copy.isEmpty()){
	    enqueue(v_copy.dequeue());
	}
    }
    return res;
}
