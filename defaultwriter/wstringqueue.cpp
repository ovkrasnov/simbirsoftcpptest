#include "wstringqueue.h"
#include <utils/misc.h>
#include <globals.h>
#include <iostream>

//TODO!!! описание
WStringQueue::WStringQueue():
    m_queue()
{}

//TODO!!! описание
WStringQueue::WStringQueue(const WStringQueue& another):
    m_queue(another.m_queue)
{}

//TODO!!! описание
WStringQueue& WStringQueue::operator=(const WStringQueue& another)
{
    if(this != &another){
	m_queue = another.m_queue;
    }
    return *this;
}

//TODO!!!описание
bool WStringQueue::operator==(const WStringQueue& another)const
{
    return (m_queue == another.m_queue);
}
//TODO!!! описание
bool WStringQueue::operator!=(const WStringQueue& another)const
{
    return !operator==(another);
}

//TODO!!! описание
size_t WStringQueue::size()const
{
    return m_queue.size();
}

//TODO!!! описание
bool WStringQueue::isEmpty()const
{
    return (size()==0);
}

//TODO!!! описание
bool WStringQueue::enqueue(const std::wstring& v)
{
    m_queue.push(v);
    return true;
}

//TODO!!! описание
bool WStringQueue::enqueue(const WStringQueue& v)
{
    bool res(true);
    WStringQueue v_copy(v);
    std::string _errstr;
    while(res && !v_copy.isEmpty()){
	res = res && utils::misc::ifFalseFill(enqueue(v_copy.dequeue()), &_errstr, FUNC_INFO("An error occured while enqueue!"));
    }
    if(!res){
	std::cout << _errstr << std::endl;
    }
    return res;
}


//TODO!!! описание
std::wstring WStringQueue::dequeue()
{
    std::wstring res;
    if(!isEmpty()){
	res = m_queue.front();
	m_queue.pop();
    }
    return res;
}

//TODO!!! описание
WStringQueue& WStringQueue::join(const std::wstring& delimiter)
{
    std::wstring res(dequeue());
    while(!isEmpty()){
	res += delimiter;
	res += dequeue();
    }
    enqueue(res);
    return *this;
}


//TODO!!! описание
void WStringQueue::clear()
{
    m_queue = std::queue<std::wstring>();
}
