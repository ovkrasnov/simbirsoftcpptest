#ifndef BWSTRINGQUEUE_H
#define BWSTRINGQUEUE_H

#include "wstringqueue.h"

class BWStringQueue : //Bounded WStringQueue
    public WStringQueue
{
    public:
	//TODO!!! описание
	BWStringQueue(size_t upperbound);
	//TODO!!! описание
	BWStringQueue(const BWStringQueue& another);
	//TODO!!! описание
	BWStringQueue& operator=(const BWStringQueue& another);

	//TODO!!!описание
	bool operator==(const BWStringQueue& another)const;
	//TODO!!! описание
	bool operator!=(const BWStringQueue& another)const;

	//TODO!!! описание
	size_t upperBound()const;
	//TODO!!! описание
	bool setUpperBound(size_t upperbound);

	//TODO!!! описание
	virtual bool enqueue(const std::wstring& v);

	//TODO!!! описание
	virtual bool enqueue(const WStringQueue& v);
    private:
	size_t m_upperbound;
};

#endif//WSTRINGQUEUE_H
