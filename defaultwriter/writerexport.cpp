#include "writerexport.h"
#include "defaultwriter.h"


bool initWriter(void** writer)
{
    bool res(true);
    res = res && writer;
    res = res && !(*writer);
    if(res){
	*writer = new DefaultWriter;
    }
    return res;
}

bool freeWriter(void **writer)
{
    bool res(true);
    res = res && writer;
    res = res && (*writer);
    if(res){
	delete (DefaultWriter*)(*writer);
	*writer = 0;
    }
    return res;
}

