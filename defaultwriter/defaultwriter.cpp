#include "defaultwriter.h"
#include <utils/string.h>
#include <utils/wstring.h>
#include <utils/misc.h>
#include <utils/parameter.h>
#include <utils/fs.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "globals.h"
#include <algorithm>

static const size_t MIN_LINES_BOUND	= 10;
static const size_t MAX_LINES_BOUND	= 100000;
static const size_t MAX_FILES_COUNT	= 99999;
static const size_t NSTRLEN		= 5;

static const std::wstring html_top =
        L"<!DOCTYPE html PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n"
        "<html>\n"
        "<head>\n"
        "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"//TODO!!! взять системную кодировку в charset
        "<title>[result]</title>\n"
        "</head>\n"
        "<body>\n"
;
//        Результат <b><i> конвертирования </i></b><br>
//        Следующая строка
static const std::wstring html_bottom =
        L"</body>\n"
        "</html>\n"
;

//TOODO!!! описание
DefaultWriter::DefaultWriter():
    AbstractWriter(				),
    //m_maxlines(			MIN_LINES_BOUND	),
    //m_currentline(		0		),
    m_outputfilename(				),
    m_dictionary(				),
    m_currentword(				),
    m_currenttext(		MIN_LINES_BOUND	),
    m_currentsentence(				),
    //m_currentsentenceline(	0		),
    m_currentfilenumber(	1		)
{}

//TODO!!! описание
DefaultWriter::~DefaultWriter()
{
    free();
}

//TODO!!! описание
bool DefaultWriter::isValid()const
{
    bool res(true);
    res = res && !outputFileName().empty();
    //res = res && currentLine()<maxLines();
    return res;
}

//TODO!!! описание
std::string DefaultWriter::usage()const
{
    static const std::string res(
	"To define library-specific options library \"defaultwriter\"\n"
	"uses parameter pairs:\n"
	"\"filename=path_to_file\" - path_to_file specifies the path prefix to generated output files;\n"
	"\"dictionary=path_to_file\" - path_to_file specifies the path to dictionary file;\n"
	"\"locale=locale_name\" - locale_name specifies the name of locale which uses to read dictionary file\n"
	"\"maxlines=max_line_count\" - max_line_count specifies max lines to write.\n"
    );
    return res;
}

//TODO!!! описание
//TODO!!! добавить заголовок и концовку html
bool DefaultWriter::write(const std::wstring& line, std::string *errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(
	line.find('\n'),
	errstr,
	FUNC_INFO("line contains line-break symbol! DefaultWriter does not support this kind of lines!")
    );//данные на вход должны подаваться построчно, без завершающего '\n' TODO!!! это обязательно отразить в описании и руководстве программиста
    if(res){
	std::wstring::const_iterator it;
	std::wstring buff_sentence;//строка, предназначенная для хранения "незакрытого" предложения текущей строки
	WStringQueue closedsentencesofline;//очередь, содержащая закрытые (точкой, восклицательным знаком и т.п.) предложения строки
	for(it = line.begin(); /*res && */it != line.end(); ++it){
	    if(utils::wstring::isWordDelimiter(*it)){
		    //std::wcout << "res=[" << res << "] current_symbol=[" << *it << "] current_word=[" << currentWord() << "]\n";
		    if(!currentWord().empty()){
			//*if(std::find(m_dictionary.begin(), m_dictionary.end(), currentWord()) != m_dictionary.end()){
			//*    buff_sentence += (L"<b><u>" + m_currentword + L"</u></b>");//по построению, слово не нуждается в переводе в html
			//*}else{
			    buff_sentence += m_currentword;//по построению, слово не нуждается в переводе в html
			//*}
			m_currentword.clear();
		    }
		    buff_sentence += htmlString(*it);
		    if(utils::wstring::isSentenceDelimiter(*it)){
			closedsentencesofline.enqueue(buff_sentence);
			buff_sentence.clear();
		    }
	    }else{
		append2CurrentWord(htmlString(*it));
	    }
	}

	if(closedsentencesofline.size() > 0){//в текущей строке было закрыто хотя-бы одно предложение?
	    //- да
	    //восстанавливаем первое предложение в очереди на запись в файл, но не забываем, что в текущей строке могли быть и другие предложения, следовавшие за первым
	    //m_currentsentence.enqueue(closedsentenceofline.dequeue());
	    if(m_currenttext.size() + m_currentsentence.size() +1 > m_currenttext.upperBound()){//в выходной файл не помещается текущий текст с первым предложением в очереди на запись
		if(m_currenttext.size() > 0){//записываем в файл текущий текст, если в нем есть хотя бы одна строка
		    res = res && flushCurrentText(errstr);
		}
		if(m_currentsentence.size()+1 > m_currenttext.upperBound()){//проверяем длину первого предложения в очереди на запись в файл - она больше ограничения на мксимальное кол-во строк в файле?
		    //- да => записываем предложение как некорректное в соответствующий файл, но не прекращаем работу
		    m_currentsentence.enqueue(closedsentencesofline.dequeue());
		    std::cout << std::string("The number of rows for current sentence violates a restriction on the maximum number of rows, so it will be written 'as is'!\n");
		    res = res && flushCurrentSentence(errstr);//как некорректное
		    if(closedsentencesofline.size()>0){
			m_currenttext.enqueue(closedsentencesofline.join().dequeue());//сохраняем оставшиеся предложения в текст
		    }
		}else{
		    //- нет => дописываем m_currentsentence (включая) в m_currenttext, затем дописываем closedsentencesofline одной строкой также в m_currenttext
		    m_currentsentence.enqueue(closedsentencesofline.join().dequeue());
		    while(m_currentsentence.size()>0){
			m_currenttext.enqueue(m_currentsentence.dequeue());
		    }
		}
	    }else{//в выходной файл помещается текущий текст со всеми предложениями в очереди на запись (включая восстановленное первое предложение очереди)
		//восстанавливаем первое предложение в очереди на запись в файл, но не забываем, что в текущей строке могли быть и другие предложение, следовавшие за первым
		m_currentsentence.enqueue(closedsentencesofline.join().dequeue());
		//запоминаем текст
		while(m_currentsentence.size() > 0){
		    m_currenttext.enqueue(m_currentsentence.dequeue());
		}
	    }
	    //m_currentsentence.clear();//не обязательно, т.к. по построению здесь он должен быть уже чистый
	    if(buff_sentence.size()>0){
		m_currentsentence.enqueue(buff_sentence);
	    }
	}else{
	    //- нет
	    //if(buff_sentence.size()>0){
		m_currentsentence.enqueue(buff_sentence);
	    //}
	}
    }
    return res;
}

//TODO!!! описание
bool DefaultWriter::flushCurrentSentence(std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(isValid(), errstr, FUNC_INFO("Writer is not valid!"));
    if(res){
	try{
	    if(m_currentsentence.size()>0){
		std::string suffix(m_currentsentence.size() > m_currenttext.upperBound() ? "abnormal_sentence.html":".html");
		std::string fn(utils::fs::generateNumberedFileName(outputFileName(), m_currentfilenumber, MAX_FILES_COUNT, NSTRLEN, ".abnormal_sentence.html"));
		std::cout << "Flushing current sentence into " << fn << "!" << std::endl;
		std::wofstream stream(fn.c_str(), std::ios::trunc);
		//*stream << html_top;
		while(m_currentsentence.size()>0){
		    stream << m_currentsentence.dequeue() << L"\n";//!L"<br />\n";
		}
		//*stream << html_bottom;
		stream.close();
	    }
	    //resetCurrentSentence();
	}catch(std::exception& e){
	    utils::misc::fill(errstr, FUNC_INFO(e.what()));
	    resetCurrentText();
	    resetCurrentSentence();
	    resetCurrentWord();
	    res = false;
	}
    }
    return res;
}


//TODO!!! описание
bool DefaultWriter::flushCurrentText(std::string* errstr)
{
    bool res(true);
    res = res && utils::misc::ifFalseFill(isValid(), errstr, FUNC_INFO("Writer is not valid!"));
    //res = res && utils::misc::ifFalseFill(!m_currenttext.empty(), errstr, FUNC_INFO("Nothing to flush!"));
    if(res){
	try{
	    if(m_currenttext.size()>0){
		std::string fn(utils::fs::generateNumberedFileName(outputFileName(), m_currentfilenumber, MAX_FILES_COUNT, NSTRLEN, ".html"));
		std::cout << "Flushing current text into " << fn << "!" << std::endl;
		std::wofstream stream(fn.c_str(), std::ios::trunc);
		//*stream << html_top;
		while(m_currenttext.size()>0){
		    stream << m_currenttext.dequeue() << L"\n";//!L"<br />\n";
		}
		//*stream << html_bottom;
		stream.close();
	    }
	    //resetCurrentText();
	    //resetCurrentSentence();
	    //resetCurrentWord();
	}catch(std::exception& e){
	    utils::misc::fill(errstr, FUNC_INFO(e.what()));
	    resetCurrentText();
	    resetCurrentSentence();
	    resetCurrentWord();
	    res = false;
	}
    }
    return res;
}

//TODO!!! описание
bool DefaultWriter::flush(std::string* errstr)
{
    bool res(true);
    if( currentText().size()+currentSentence().size() > m_currenttext.upperBound()){//если currentText(), объединенный с currentSentence() не влезают в ограничение на количество строк в выходном файле
	res = res && flushCurrentText(		errstr);
	res = res && flushCurrentSentence(	errstr);
    }else{//иначе, объединяем их и записываем в один файл
	m_currenttext.enqueue(m_currentsentence);
	res = res && flushCurrentText(errstr);
    }
    m_currenttext	.clear();//на всякий случай
    m_currentsentence	.clear();//на всякий случай
    return res;
}

//TODO!!! описание
bool DefaultWriter::loadOptions(const STDStringMultiMap& opts, std::string *errstr)
{
    bool res(true);
    std::string filename;
    std::string dictionary;
    std::string dict_locale;
    std::string maxlines;
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "filename", &filename),
	errstr,
	FUNC_INFO("Parameter 'filename' is not specified!")
    );
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "dictionary", &dictionary),
	errstr,
	FUNC_INFO("Parameter 'dictionary' is not specified!")
    );
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "locale", &dict_locale),
	errstr,
	FUNC_INFO("Parameter 'locale' is not specified!")
    );
    res = res && utils::misc::ifFalseFill(
	utils::parameter::getValueByName(opts, "maxlines", &maxlines),
	errstr,
	FUNC_INFO("Parameter 'maxlines' is not specified!")
    );
    res = res && init(filename, dictionary, dict_locale, maxlines, errstr);
    return res;
}

//TODO!!! описание
bool DefaultWriter::init(
    const std::string& output_filename,
    const std::string& dictionary_filename,
    const std::string& dictionary_localename,
    const std::string& maxlines,
    std::string *errstr
)
{
    bool res(true);
    Dictionary	buff_dict;
    free();//на всякий случай
    res = res && tryInitDictionary(dictionary_filename, dictionary_localename, &buff_dict, errstr);
    res = res && tryInitCurrentText(maxlines, errstr);
    if(res){
	m_outputfilename	= output_filename;
	m_dictionary		= buff_dict;
    }
    return res;
}

//TODO!!! описание
bool DefaultWriter::tryInitDictionary(const std::string& filename, const std::string& localename, Dictionary* dst, std::string* errstr)
{
    bool res(true);
    try{
	res = res && utils::misc::ifFalseFill(dst, errstr, FUNC_INFO("Parameter 'dst' is null!"));
	res = res && utils::misc::ifFalseFill(
	    utils::fs::fileSizeInBytes(filename)<=utils::fs::MaxFileSizeInBytes,
	    errstr,
	    FUNC_INFO(std::string()
		+ "The dictionary file size is greater than "
		+ utils::string::toStdString(utils::fs::MaxFileSizeInBytes)
		+ "bytes!"
	    )
	);
	if(res){
	    std::wifstream stream;
	    std::locale locale(localename.c_str());
	    stream.imbue(locale);
	    std::wstring buff;
	    int line(0);stream.open(filename.c_str());
	    while(std::getline(stream, buff)){
		line++;
		bool ok(true);
		for(std::wstring::const_iterator it = buff.begin(); it != buff.end() && ok; ++it){//проверяем то, что словарь содержит в строках по одному слову
		    ok = ok && utils::wstring::isNotWordDelimiter(*it);
		}
		if(ok){
		    dst->push_back(/*htmlString(*/buff/*)*/);//выполнять htmlString необязательно, т.к.
		                                                //isNotWordDelimiter исключает символы,
		                                                //которые заменяются в DefaultWriter::htmlString
		}else{
		    std::cerr << FUNC_INFO("Warning incorrect word in line '" +utils::string::toStdString(line)+"\'!");
		}
	    }
	    std::sort(dst->begin(), dst->end());
	}
    }catch(std::exception& e){
	res = false;
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
    }

    return res;
}

//TODO!!! описание
bool DefaultWriter::tryInitCurrentText(const std::string& maxlines, std::string* errstr)
{
    bool res(true);
    try{
	unsigned int uimaxlines(abs(utils::string::toInt(maxlines, &res)));
	if(res){
	    res = res && utils::misc::ifFalseFill(uimaxlines >= MIN_LINES_BOUND && uimaxlines <= MAX_LINES_BOUND, errstr, FUNC_INFO(std::string() + "Out of bounds error!"));
	    res = res && utils::misc::ifFalseFill(m_currenttext.setUpperBound(uimaxlines), errstr, FUNC_INFO(std::string() + "Can't set upper bound to '" + maxlines + "'!"));
	}else{
	    utils::misc::fill(errstr, FUNC_INFO(std::string() + "Can't convert string '" + maxlines + "' to integer!"));
	}
    }catch(std::exception& e){
	res = false;
	utils::misc::fill(errstr, FUNC_INFO(e.what()));
    }
    return res;
}


//TODO!!! описание
void DefaultWriter::resetCurrentText()
{
    m_currenttext.clear();
}
//TODO!!! описание
void DefaultWriter::resetCurrentSentence()
{
    m_currentsentence.clear();
}
//TODO!!! описание
void DefaultWriter::resetCurrentWord()
{
    m_currentword.clear();
}

//TODO!!! описание
void DefaultWriter::append2CurrentSentence(const std::wstring& htmltext)
{
    m_currentsentence.enqueue(htmltext);
}

//TODO!!! описание
void DefaultWriter::append2CurrentText(const std::wstring& htmltext)
{
    m_currenttext.enqueue(htmltext);
}

//TODO!!! описание
void DefaultWriter::append2CurrentWord(const std::wstring& htmltext)
{
    m_currentword += htmltext;
}

//TODO!!! описание
/*
size_t DefaultWriter::maxLines()const
{
    return m_maxlines;
}
*/
//TODO!!! описание
/*
size_t DefaultWriter::currentLine()const
{
    return m_currentline;
}
*/
//TODO!!! описание
std::wstring  DefaultWriter::currentWord()const
{
    return m_currentword;
}
//TODO!!! описание
BWStringQueue  DefaultWriter::currentText()const
{
    return m_currenttext;
}

//TODO!!! описание
WStringQueue  DefaultWriter::currentSentence()const
{
    return m_currentsentence;
}

//TODO!!! описание
std::string DefaultWriter::outputFileName()const
{
    return m_outputfilename;
}

void DefaultWriter::free()
{
    m_currenttext.setUpperBound(MIN_LINES_BOUND);
    m_outputfilename    .clear();
    m_dictionary        .clear();
    resetCurrentWord();
    resetCurrentText();
    resetCurrentSentence();
    m_currentfilenumber = 1;
}

std::wstring DefaultWriter::htmlString(wchar_t c)
{
    return htmlString(std::wstring()+c);
}


std::wstring DefaultWriter::htmlString(const std::wstring& src)
{
    std::wstring res(src);
    //*utils::wstring::replace(&res, L"&"    , L"&amp;"   );
    //*utils::wstring::replace(&res, L"<"    , L"&lt;"    );
    //*utils::wstring::replace(&res, L">"    , L"&gt;"    );
    //*utils::wstring::replace(&res, L"\""   , L"&quot;"  );
    //*utils::wstring::replace(&res, L"'"    , L"&apos;"  );
    //*utils::wstring::replace(&res, L" "    , L"&nbsp;"  );
    //*utils::wstring::replace(&res, L"\t"   , L"&nbsp;&nbsp;&nbsp;&nbsp;");
    return res;
}
